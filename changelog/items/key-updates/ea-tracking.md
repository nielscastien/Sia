- Track EA spending through the account spending details to see what the money
  from an ephemeral account is being spent on.
